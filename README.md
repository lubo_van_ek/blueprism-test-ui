# If I had more time I would consider doing these

- Filtering
- Better splitting of logic and application layer
- Nicer UI with icons - some parts of the UI would look much better if they had icons rather then text (e. g. status like terminated, pedning etc.)
- Better accessibility - if needed and there is enough time it is good to go through the app with a screen reader to make sure it is fully accessible
- Lazy loading - at this point I would start using React Query to handle this for me
- Loading state and error handling - React Query would help here as it returns the state that I could act upon

## What I wouldn't do if I had more time

- UI testing - I don't think UI applications should have dedicated UI tests (those should be handled by integration tests). It would make the code brittle and hard to change (every time I see a company doing this, making changes to the code becames a chore).

## Notes

- `.env` file should not be part of the repository, I am only including it for completeness
- Some properties of the Schedule are not displayed because I wasn't sure if they are actually bringing any value. This would be different on an actualy project as I would have a point of contact to discuss this with.
