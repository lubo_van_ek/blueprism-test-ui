import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import ScheduleContextProvider from "./components/ScheduleContextProvider/ScheduleContextProvider";

ReactDOM.render(
  <React.StrictMode>
    <ScheduleContextProvider>
      <App />
    </ScheduleContextProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
