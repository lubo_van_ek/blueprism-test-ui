import { Dispatch, SetStateAction } from "react";

export interface IScheduleContext {
  selectedSchedule: number;
  setSelectedSchedule: Dispatch<SetStateAction<number>>;
}
