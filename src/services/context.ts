import { createContext } from "react";
import { IScheduleContext } from "../interfaces/IScheduleContext";

export const ScheduleContext = createContext<IScheduleContext>({
  selectedSchedule: 0,
  setSelectedSchedule: () => {},
});
