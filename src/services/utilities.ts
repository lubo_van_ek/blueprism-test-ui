import { Schedule } from "../interfaces/Schedule";

export const NOT_SELECTED = -1;

export const getSelectedId = (
  schedules: Schedule[],
  selectedIndex: number
): number =>
  schedules.length > 0 ? schedules[selectedIndex].id : NOT_SELECTED;
