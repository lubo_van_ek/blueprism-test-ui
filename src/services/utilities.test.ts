import { Schedule } from "../interfaces/Schedule";
import { getSelectedId, NOT_SELECTED } from "./utilities";

const schedules: Schedule[] = [
  {
    id: 3,
    dayOfMonth: 0,
    dayOfWeek: 0,
    description: "",
    endDate: "",
    endPoint: "",
    intervalType: "",
    isRetired: false,
    name: "",
    startDate: "",
    startPoint: "",
    tasksCount: 0,
    timePeriod: 0,
  },
  {
    id: 4,
    dayOfMonth: 0,
    dayOfWeek: 0,
    description: "",
    endDate: "",
    endPoint: "",
    intervalType: "",
    isRetired: false,
    name: "",
    startDate: "",
    startPoint: "",
    tasksCount: 0,
    timePeriod: 0,
  },
];

test("GetSelectedId returns correct number when first is selected", async () => {
  const selectedIndex = 0;
  const expectedId = 3;

  const result = getSelectedId(schedules, selectedIndex);

  expect(result).toEqual(expectedId);
});

test("GetSelectedId returns correct number when second is selected", async () => {
  const selectedIndex = 1;
  const expectedId = 4;
  const result = getSelectedId(schedules, selectedIndex);

  expect(result).toEqual(expectedId);
});

test("GetSelectedId returns -1 (NOT_SELECTED) when schedules are empty", async () => {
  const selectedIndex = 0;
  const expectedId = NOT_SELECTED;
  const result = getSelectedId([], selectedIndex);

  expect(result).toEqual(expectedId);
});
