import { FC, useContext } from "react";
import { Schedule } from "../../interfaces/Schedule";
import { ScheduleContext } from "../../services/context";
import ScheduleCard from "./ScheduleCard/ScheduleCard";
import styles from "./Schedules.module.scss";

const Schedules: FC<{ schedules: Schedule[] }> = ({ schedules }) => {
  const { selectedSchedule, setSelectedSchedule } = useContext(ScheduleContext);
  return (
    <div className={styles.container}>
      {schedules.map((schedule: Schedule, index: number) => (
        <ScheduleCard
          key={schedule.id}
          schedule={schedule}
          isSelected={selectedSchedule === index}
          select={() => setSelectedSchedule(index)}
        />
      ))}
    </div>
  );
};

export default Schedules;
