import axios from "axios";
import { FC, useState } from "react";
import { Schedule } from "../../../interfaces/Schedule";
import styles from "./ScheduleCard.module.scss";

const ScheduleCard: FC<{
  schedule: Schedule;
  isSelected: boolean;
  select: () => void;
}> = ({ schedule, isSelected, select }) => {
  const [isRetired, setIsRetired] = useState(schedule.isRetired);
  const handleRetire = (): void => {
    const toggleRetire = async (): Promise<void> => {
      const response = await axios.put<Schedule>(
        `${process.env.REACT_APP_API}/schedules/${schedule.id}`,
        {
          ...schedule,
          isRetired: !isRetired,
        }
      );
      if (response.status === 200) {
        setIsRetired(response.data.isRetired);
      }
    };
    toggleRetire();
  };

  const selectedClass = isSelected ? styles.selected : "";
  return (
    <div className={`${styles.container}`}>
      <button
        className={`${styles.mainButton} ${selectedClass}`}
        type="button"
        onClick={select}
      >
        <h3>{schedule.name}</h3>
        <div>{schedule.description}</div>
        <div className={styles.inline}>
          <div>
            Start: {new Date(schedule.startDate).toLocaleDateString("en-GB")}
          </div>
          <div>
            End: {new Date(schedule.endDate).toLocaleDateString("en-GB")}
          </div>
        </div>
      </button>
      <button
        type="button"
        className={styles.smallButton}
        onClick={handleRetire}
      >
        {isRetired ? "Unretire" : "Retire"}
      </button>
    </div>
  );
};

export default ScheduleCard;
