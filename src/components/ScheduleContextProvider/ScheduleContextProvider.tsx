import { useState } from "react";
import { ScheduleContext } from "../../services/context";

const SearchContextProvider: React.FC = ({ children }) => {
  const [selectedSchedule, setSelectedSchedule] = useState<number>(0);

  return (
    <ScheduleContext.Provider value={{ selectedSchedule, setSelectedSchedule }}>
      {children}
    </ScheduleContext.Provider>
  );
};

export default SearchContextProvider;
