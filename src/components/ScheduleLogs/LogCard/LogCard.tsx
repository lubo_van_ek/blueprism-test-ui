import { FC } from "react";
import { ScheduleLog } from "../../../interfaces/ScheduleLog";
import styles from "./LogCard.module.scss";

const LogCard: FC<{ log: ScheduleLog }> = ({ log }) => (
  <div className={styles.container}>
    <h3>{log.serverName}</h3>
    <div className={styles.status}>{log.status}</div>
    <div className={styles.inline}>
      <div>Start: {new Date(log.startTime).toLocaleDateString("en-GB")}</div>
      <div>End: {new Date(log.endTime).toLocaleDateString("en-GB")}</div>
    </div>
  </div>
);

export default LogCard;
