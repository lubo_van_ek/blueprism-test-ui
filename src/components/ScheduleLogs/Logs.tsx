import { FC } from "react";
import { ScheduleLog } from "../../interfaces/ScheduleLog";
import LogCard from "./LogCard/LogCard";
import styles from "./Logs.module.scss";

const Logs: FC<{ logs: ScheduleLog[] }> = ({ logs }) => (
  <div className={styles.container}>
    {logs.map((log: ScheduleLog) => (
      <LogCard key={log.id} log={log} />
    ))}
  </div>
);

export default Logs;
