import { FC } from "react";
import styles from "./Header.module.scss";

const Header: FC = () => (
  <header className={styles.header}>
    <h1>Schedules</h1>
  </header>
);

export default Header;
