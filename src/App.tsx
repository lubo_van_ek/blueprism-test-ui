import { FC, useContext, useEffect, useState } from "react";
import axios from "axios";
import { Schedule } from "./interfaces/Schedule";
import Schedules from "./components/Schedules/Schedules";
import { ScheduleLog } from "./interfaces/ScheduleLog";
import Logs from "./components/ScheduleLogs/Logs";
import styles from "./App.module.scss";
import Header from "./components/Header/Header";
import { getSelectedId } from "./services/utilities";
import { ScheduleContext } from "./services/context";

const App: FC = () => {
  const [schedules, setSchedules] = useState<Schedule[]>([]);
  const [scheduleLogs, setScheduleLogs] = useState<ScheduleLog[]>([]);
  const { selectedSchedule } = useContext(ScheduleContext);

  useEffect(() => {
    const fetchSchedules = async (): Promise<void> => {
      const { data }: { data: Schedule[] } = await axios.get<Schedule[]>(
        `${process.env.REACT_APP_API}/schedules`
      );
      setSchedules(data);
    };
    fetchSchedules();
  }, []);

  useEffect(() => {
    const selectedId = getSelectedId(schedules, selectedSchedule);
    const fetchScheduleLogs = async (): Promise<void> => {
      const { data }: { data: ScheduleLog[] } = await axios.get<ScheduleLog[]>(
        `${process.env.REACT_APP_API}/scheduleLogs?scheduleId=${selectedId}`
      );
      setScheduleLogs(data);
    };
    fetchScheduleLogs();
  }, [schedules, selectedSchedule]);

  return (
    <>
      <Header />
      <div className={styles.container}>
        <Schedules schedules={schedules} />
        <Logs logs={scheduleLogs} />
      </div>
    </>
  );
};

export default App;
